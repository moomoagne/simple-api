Simple API
==========

A Symfony based API created on October 8, 2018, 6:34 pm.

##Installation
1. Clone the repository
  ``git clone https://gitlab.com/m3doune/simple-api.git``
2. Install dependencies with `composer`, using this command `composer install`
3. Run migrations with ``php bin/console doctrine:migrations:migrate``.
4. Load fake data fixtures with `php bin/console doctrine:fixtures:load`.
5. Launch built-in server with ``php bin/console server:run``
6. Checkout the API documentation at: [http://127.0.0.1:8000/api/doc](http://127.0.0.1:8000/api/doc)