<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Brand;
use AppBundle\Entity\Category;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFixtures implements FixtureInterface {

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $brands=['HERMES', 'PRADA','CHANEL','RALPHLAUREN','BURBERRY','HOUSEOFVERSACE','FENDI','ARMANI'];

        foreach ($brands as $brand) {
            $br = (new Brand())->setName($brand);
            $manager->persist($br);
        }

        $categories= ['Pantalon', 'Chemise', 'Chaussure de ville', 'Accessoires'];
        foreach ($categories as $category) {
            $manager->persist((new Category())->setName($category));
        }

        $manager->flush();
    }
}