<?php
/**
 * Created by PhpStorm.
 * User: medoune
 * Date: 15/01/2018
 * Time: 21:56
 */

namespace AppBundle\Exception;


use Symfony\Component\Form\FormInterface;

class InvalidFormException extends \Exception
{
    const DEFAULT_ERROR_MESSAGE = "Les données postées sont invalides";

    protected $form;

    /**
     * InvalidFormException constructor.
     * @param FormInterface $form
     * @param string $message
     * @internal param int $code
     * @internal param Exception|null $previous
     */
    public function __construct(FormInterface $form = null, $message = self::DEFAULT_ERROR_MESSAGE)
    {
        parent::__construct($message);
        $this->form = $form;

    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }
}