<?php

namespace AppBundle\Controller;

use AppBundle\Exception\InvalidFormException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductController extends BaseApiController
{
    /**
     * @return \AppBundle\Manager\ProductManager|object
     */
    protected function getManager()
    {
        return $this->get('app.manager.product_manager');
    }

    /**
     * @param Request $request
     * @Rest\Get("/products/{id}", requirements={"id":"\d+"}, name="get_single_product")
     * @return mixed|null
     * @ApiDoc(
     *     section="Product",
     *     description="Return a single Product",
     *     resource=true,
     *     statusCodes={
     *      200="Returned when successful",
     *      404="Returned when not found",
     *      500="Returned when something get wrong"
     *     })
     */
    public function getAction(Request $request)
    {
        $id = $request->get('id');
        $product = $this->getManager()->get($id);

        if(!$product){
            $this->throwProductNotFoundException();
        }

        return $product;

    }


    /**
     * @param Request $request
     * @Rest\Get("/products",name="get_all_products")
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     * @ApiDoc(
     *     section="Product",
     *     description="Return a list of Products",
     *     resource=true,
     *     statusCodes={
     *      200="Returned when successful",
     *      500="Returned when something get wrong"
     *     },
     *     filters={
     *          {"name"="page", "dataType"="integer", "default"="1"},
     *          {"name"="limit", "dataType"="integer", "default"="10"},
     *     })
     */
    public function cgetAction(Request $request)
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 1);

        return $this->getManager()->all($page, $limit);
    }


    /**
     * @param Request $request
     * @Rest\Post("/products", name="post_a_new_product")
     * @return mixed|JsonResponse
     * @ApiDoc(
     *     section="Product",
     *     description="Post a new Product",
     *     resource=true,
     *     statusCodes={
     *      200="Returned when successful",
     *      400="Returned when form data are invalid",
     *      500="Returned when something else get wrong"
     *     },
     *  parameters={
     *      {"name"="name", "dataType"="string", "required"=true, "description"="product name"},
     *      {"name"="description", "dataType"="textarea", "required"=true, "description"="product description"},
     *      {"name"="brand", "dataType"="integer", "required"=true, "description"="brand id"}
     *  },
     *     output="AppBundle\Entity\Product"
     * )
     */
    public function postAction(Request $request)
    {
        $parameters = $request->request->all();
        $options = ['method' => $request->getMethod()];

        try{
            return $this->getManager()->post($parameters, $options);
        }catch (InvalidFormException $exception){
            return $this->returnSerializedFormError($exception);
        }

    }


    /**
     * @param Request $request
     * @Rest\Patch("/products/{id}", requirements={"id": "\d+"}, name="update_product_route")
     * @return mixed|JsonResponse
     * @ApiDoc(
     *     section="Product",
     *     description="Update an existing Product",
     *     resource=true,
     *     statusCodes={
     *      200="Returned when successful",
     *      400="Returned when form data are invalid",
     *      404="Returned when product not found",
     *      500="Returned when something else get wrong"
     *     },
     *  parameters={
     *      {"name"="name", "dataType"="string", "required"=true, "description"="product name"},
     *      {"name"="description", "dataType"="textarea", "required"=true, "description"="product description"},
     *      {"name"="brand", "dataType"="integer", "required"=true, "description"="brand id"}
     *  },
     *     output="AppBundle\Entity\Product"
     * )
     */
    public function patchAction(Request $request)
    {
        $productId = $request->get('id');

        $product = $this->getManager()->get($productId);

        if(!$product){
            $this->throwProductNotFoundException();
        }

        try{
            $parameters = $request->request->all();
            $options = ['method' => $request->getMethod()];
            return $this->getManager()->patch($product, $parameters, $options);
        }catch (InvalidFormException $exception){
            return $this->returnSerializedFormError($exception);
        }
    }


    /**
     * @param Request $request
     * @Rest\Delete("/products/{id}", requirements={"id": "\d+"}, name="delete_product_route")
     * @return JsonResponse
     * @ApiDoc(
     *     section="Product",
     *     description="Delete an existing Product",
     *     resource=true,
     *     statusCodes={
     *      204="Returned when successful",
     *      404="Returned when product is not found",
     *      500="Returned when something else get wrong"
     *     },
     * )
     */
    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $product = $this->getManager()->get($id);

        if(!$product){
            $this->throwProductNotFoundException();
        }

        $this->getManager()->delete($product);
        return $this->json(null, JsonResponse::HTTP_NO_CONTENT);

    }


    private function throwProductNotFoundException()
    {
        $message = 'Product not found';
        throw new NotFoundHttpException($message);
    }

    /**
     * @param $exception
     * @return JsonResponse
     */
    protected function returnSerializedFormError(InvalidFormException $exception): JsonResponse
    {
        $serializer = $this->get('jms_serializer');
        $form = $serializer->serialize($exception->getForm(), 'json');
        return $this->json($form, JsonResponse::HTTP_BAD_REQUEST);
    }

}
