<?php
/**
 * Created by PhpStorm.
 * User: medoune
 * Date: 10/8/18
 * Time: 11:44 PM
 */

namespace AppBundle\Controller;


use AppBundle\Exception\InvalidFormException;
use AppBundle\Manager\BaseManager;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class BaseApiController extends FOSRestController
{
    /**
     * @return BaseManager
     */
    abstract protected function getManager();

    /**
     * @param Request $request
     * @return mixed
     * Return single object
     */
    abstract public function getAction(Request $request);

    /**
     * @param Request $request
     * @return mixed
     * Return collection
     */
    abstract public function cgetAction(Request $request);


    /**
     * @param Request $request
     * @return mixed
     */
    abstract public function postAction(Request $request);


    /**
     * @param Request $request
     * @return mixed
     */
    abstract public function patchAction(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    abstract public function deleteAction(Request $request);

    /**
     * @param $exception
     * @return JsonResponse
     */
    protected function returnSerializedFormError(InvalidFormException $exception): JsonResponse
    {
        $serializer = $this->get('jms_serializer');
        $form = $serializer->serialize($exception->getForm(), 'json');
        return $this->json($form, JsonResponse::HTTP_BAD_REQUEST);
    }

}