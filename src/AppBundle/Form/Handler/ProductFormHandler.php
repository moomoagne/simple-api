<?php
/**
 * Created by PhpStorm.
 * User: medoune
 * Date: 10/8/18
 * Time: 8:30 PM
 */

namespace AppBundle\Form\Handler;


use AppBundle\Entity\Product;
use AppBundle\Exception\InvalidFormException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProductFormHandler extends BaseFormHandler
{
    protected $urlGenerator;

    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $formFactory, FormTypeInterface $formType, UrlGeneratorInterface $urlGenerator)
    {
        parent::__construct($entityManager, $formFactory, $formType);
        $this->urlGenerator = $urlGenerator;
    }

    public function processForm($object, array $parameters, $method)
    {
        /** @var Product $product */
        $product = parent::processForm($object, $parameters, $method);
        $productUrl = $this->urlGenerator->generate('getget_single_product', ['id' => $product->getId()]);
        $product->setUrl($productUrl);

        $this->save($product);
        $this->flush();

        return $product;

    }

}