<?php
/**
 * Created by PhpStorm.
 * User: medoune
 * Date: 15/01/2018
 * Time: 21:39
 */

namespace AppBundle\Form\Handler;


use AppBundle\Exception\InvalidFormException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormTypeInterface;

class BaseFormHandler
{
    protected $entityManager;
    protected $formFactory;
    protected $formType;

    /**
     * BaseFormHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param FormFactoryInterface $formFactory
     * @param FormTypeInterface $formType
     */
    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $formFactory, FormTypeInterface $formType)
    {
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
        $this->formType = $formType;
    }

    /**
     * @param $object
     * @param array $parameters
     * @param $method
     * @return mixed Process the form with data received from the request
     *
     * Process the form with data received from the request
     * @throws InvalidFormException
     */
    public function processForm($object, array $parameters, $method)
    {
        $form = $this->formFactory->create(get_class($this->formType), $object,[
            'method' => $method,
            'csrf_protection' => false
        ]);

        $form->submit($parameters, 'PATCH' !== $method);

        if(!$form->isValid()){
            throw new InvalidFormException($form);

        }

        $data = $form->getData();

        $this->save($data);
        $this->flush();

        return  $data;


    }

    /**
     * @param $object
     * @return mixed Save an object with an object manager
     * Save an object with an object manager
     * @internal param bool $withFlush
     */
    public function save($object)
    {
        $this->entityManager->persist($object);
        
        return $object;
    }

    public function flush()
    {
        $this->entityManager->flush();
        return true;
    }

    /**
     * @param $object
     * @return mixed Delete an object with the object manager
     * Delete an object with the object manager
     * @internal param bool $withFlush
     */
    public function delete($object)
    {
        $this->entityManager->remove($object);
        $this->flush();

        return true;
    }

}